<?php

/*
Plugin Name: Experience Camping - RV Sales API
Description: WordPress plugin containing library functions for Experience Camping RV Sales.
Author: Eric Den Hollander
Author URI: mailto:eric@denhollander.ca
Version: 2.5.0

Version History
2.0.0 2019-02-09 Initial release; separated from the Windmill Family Campground Library plugin with changes.
2.0.1 2019-02-11 Removed comments support.
2.1.0 2019-02-12 Added PDPMailer vendor library to support email submissions; removed dashboard metaboxes.
2.1.1 2019-02-14 Added Google Analytics reporting to trailers listing page; removed comments metaboxes from trailer edit page.
2.1.2 2019-02-15 Removed dashboard widgets.
2.2.0 2019-02-20 Added monthly, biweekly, biweekly with warranty payment custom fields to trailer edit page; trailer list page revisions.
2.2.1 2019-03-29 Changed logo on WordPress login page to Experience Camping.
2.3.0 2019-05-06 Changed Windimll Trailer Sales to Experience Camping RV Sales.
2.4.0 2019-10-29 Added ability for trailers to be ordered (sorted) by the user.
2.5.0 2019-11-29 Added trailer sales marketing and regular URLs to settings page.
2.6.0 2021-03-01 Added trailer ID and new/used sale type.
2.6.1 2021-03-19 Added trailer ID and new/used sale type to admin list.
*/


// define plugin version number
define("TRAILER_SALES_LIB_PLUGIN_VERSION", "2.6.1");

// define plugin location constants
define("TRAILER_SALES_LIB_PLUGIN_PATH", __DIR__ . "/");
define("TRAILER_SALES_LIB_PLUGIN_FILE", __FILE__);

// define photo locations
define("TRAILER_SALES_LIB_PHOTOS_PATH", wp_upload_dir()["basedir"] . "/trailer-sales-lib/");
define("TRAILER_SALES_LIB_PHOTOS_URL", wp_upload_dir()["baseurl"] . "/trailer-sales-lib/");

// instantiate plugin
include_once TRAILER_SALES_LIB_PLUGIN_PATH . "TrailerSalesLib.php";
$campgroundLibPlugin = new TrailerSalesLib\TrailerSalesLib();
