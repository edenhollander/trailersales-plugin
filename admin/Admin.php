<?php

namespace TrailerSalesLib\Admin;

/**
 * Admin
 * =====
 * Contains Trailer Sales API customizations for the WordPress administration.
 * @author Eric Den Hollander
 */
class Admin {
	
	private $trailersPostList;
	private $locationsPostList;
	private $manufacturersPostList;
	
	private $trailerPostEdit;
	private $locationPostEdit;
	private $manufacturerPostEdit;
	
	private $settings;	
	
	/**
	 * Adds all WordPress administration customizations.
	 */
	public function __construct() {
		if (is_admin()) {
			$this->addActionHooks();
			$this->addPostListCustomizations();
			$this->addPostEditCustomizations();
			$this->addPages();			
		}
	}
	
	/**
	 * Adds any WordPress action hooks that are needed for WordPress administration customizations.
	 */
	public function addActionHooks() {
		add_action("admin_init", [$this, "addCapabilities"]);		
		add_action("admin_enqueue_scripts", [$this, "enqueueAssets"]); 
		add_action("admin_init", [$this, "addAdminMenuSeparator"]);
		add_action("admin_menu", [$this, "setAdminMenuSeparator"]);
		add_action("admin_menu", [$this, "changeAdminMenuOrder"]);
		add_action("admin_menu", [$this, "removeAdminMenuItems"], 99);		
		add_filter("admin_bar_menu", [$this, "changeHowdyText"], 10, 3);
		add_action("wp_dashboard_setup", [$this, "removeDashboardWidgets"]);	
		add_filter("use_block_editor_for_post_type", [$this, "disableBlockEditor"], 10, 2);			
		add_filter("admin_menu", [$this, "removePageMetaBoxes"]);
		add_filter("get_user_option_meta-box-order_page", [$this, "orderPageMetaBoxes"]);	
		add_filter("default_hidden_meta_boxes", [$this, "hideDefaultMetaBoxes"], 10, 2);
		add_action("admin_init", [$this, "changePageLabels"]);	
		add_filter("page_row_actions", [$this, "hidePageQuickEdit"], 10, 2 );			
		add_filter("wpseo_metabox_prio", [$this, "prioritizeSeoMetaBox"]);
		add_filter("post_updated_messages", [$this, "changePostSaveMessages"], 0);
		remove_action("welcome_panel", "wp_welcome_panel");		
	}
	
	/**
	 * Adds any WordPress administration customizations for the custom post types for the list post view.
	 */
	private function addPostListCustomizations() {
		include_once TRAILER_SALES_LIB_PLUGIN_PATH . "admin/post-list/Trailers.php";
		include_once TRAILER_SALES_LIB_PLUGIN_PATH . "admin/post-list/Locations.php";
		include_once TRAILER_SALES_LIB_PLUGIN_PATH . "admin/post-list/Manufacturers.php";
		$this->trailersPostList = new PostList\Trailers();
		$this->locationsPostList = new PostList\Locations();
		$this->manufacturersPostList = new PostList\Manufacturers();
	}
	
	/**
	 * Adds any WordPress administration customizations for the custom post types for the edit post view.
	 */
	private function addPostEditCustomizations() {
		include_once TRAILER_SALES_LIB_PLUGIN_PATH . "admin/post-edit/Trailer.php";
		include_once TRAILER_SALES_LIB_PLUGIN_PATH . "admin/post-edit/Location.php";
		include_once TRAILER_SALES_LIB_PLUGIN_PATH . "admin/post-edit/Manufacturer.php";
		$this->trailerPostEdit = new PostEdit\Trailer();
		$this->locationPostEdit = new PostEdit\Location();
		$this->manufacturerPostEdit = new PostEdit\Manufacturer();
	}

	/**
	 * Adds any pages such as the settings options page to the WordPress administration.
	 */
	private function addPages() {
		include_once TRAILER_SALES_LIB_PLUGIN_PATH . "admin/Settings.php";
		$this->settings = new Settings();
	}
		
	/**
	 * Adds any custom capabilities to user roles.
	 */
	public function addCapabilities() {
		// allow the administrator and editor roles to manage the Trailer Sales settings page using the "manage_trailer_sales_settings" capability
		$roles = ["administrator", "editor"];
		foreach ($roles as $role) {
			$wpRole = get_role($role);
			$wpRole->add_cap("manage_trailer_sales_settings"); 
		}
	}	
	
	/**
	 * Enqueues JavaScript and CSS assets needed for the WordPress administration.
	 */
	public function enqueueAssets() {
		wp_enqueue_style("trailer-sales-admin-css", plugins_url("/trailer-sales-lib/admin/assets/admin.css"), array(), TRAILER_SALES_LIB_PLUGIN_VERSION);
		wp_enqueue_style("trailer-sales-fonts-css", plugins_url("/trailer-sales-lib/admin/assets/fonts.css"), array(), TRAILER_SALES_LIB_PLUGIN_VERSION);
	}
	
	/**
	 * Runs the "admin_init" action to set the admin menu separator.
	 */
	public function setAdminMenuSeparator() {
		do_action("admin_init");
	}
	
	/**
	 * Add separator to the WordPress administration menu (below "pages" and above custom post types).
	 * @global mixed[] $menu The WordPress menu structure.
	 */
	public function addAdminMenuSeparator() {
		global $menu;
		$menu[30] = array(
			0 => "",
			1 => "read",
			2 => "separator" . 30,
			3 => "",
			4 => "wp-menu-separator"
		);
	}
	
	/**
	 * Changes the order of the WordPress administration menu.
	 * @global mixed[] $menu The WordPress menu structure.
	 */
	public function changeAdminMenuOrder() {
		global $menu;
		
		// swap "posts" and "media"
		$mediaMenu = $menu[10];
		$postsMenu = $menu[5];
		$menu[5] = $mediaMenu;
		$menu[10] = $postsMenu;
	}
	
	/**
	 * Removes items from the WordPress administration menu.
	 * @global mixed[] $submenu Associative array containing all the WordPress submenu items.
	 */
	public function removeAdminMenuItems() {
		global $submenu;
		
		remove_menu_page("edit.php");		
		remove_menu_page("edit-comments.php");		
		if ((isset($submenu["tools.php"])) && (count($submenu["tools.php"]) <= 1)) {
			remove_menu_page("tools.php");
		}
	}	
	
	/**
	 * Changes the greeting text from "Howdy" to "Welcome" in the WordPress admin bar.
	 * @param WP_Admin_Bar $wpAdminBar The WordPress administration bar object (passed by reference).
	 */
	public function changeHowdyText($wpAdminBar) {
		$myAccount = $wpAdminBar->get_node("my-account");
		$newText = str_replace("Howdy", "Welcome", $myAccount->title);
		$wpAdminBar->add_node(array("id" => "my-account", "title" => $newText));
	}
	
	/**
	 * Removes dashboard widgets from the WordPress dashboard page.
	 * @global mixed[] $wp_meta_boxes Associative array containing the dashboard metaboxes.
	 */
	public function removeDashboardWidgets() {
		global $wp_meta_boxes;

		unset($wp_meta_boxes["dashboard"]["normal"]["core"]["dashboard_incoming_links"]);
		unset($wp_meta_boxes["dashboard"]["side"]["core"]["dashboard_primary"]); 
		unset($wp_meta_boxes["dashboard"]["normal"]["core"]["dashboard_activity"]);
		unset($wp_meta_boxes["dashboard"]["normal"]["core"]["dashboard_recent_comments"]);
		unset($wp_meta_boxes["dashboard"]["side"]["core"]["dashboard_quick_press"]);
		unset($wp_meta_boxes["dashboard"]["side"]["core"]["dashboard_recent_drafts"]);		
	}	

	/**
	 * Disables block editor for all post types.
	 * @param boolean $useBlockEditor Whether the post type can be edited or not (default true).
	 * @param string $postType The post type being checked.
	 */
	public function disableBlockEditor($useBlockEditor, $postType) {
		return false;
	}
	
	/**
	 * Removes the specified meta boxes from the default "page" post type.
	 */
	public function removePageMetaBoxes() {
		// remove_meta_box("authordiv", "page", "normal");
		remove_meta_box("commentstatusdiv", "page", "normal");
		remove_meta_box("commentsdiv", "page", "normal");
		remove_meta_box("postcustom", "page", "normal");
		// remove_meta_box("postexcerpt", "page", "normal");
		// remove_meta_box("revisionsdiv", "page", "normal");
		// remove_meta_box("slugdiv", "page", "normal");	
		remove_meta_box("trackbacksdiv", "page", "normal");
	}
	
	/**
	 * Orders the meta boxes for the "page" post type.
	 * @param mixed[] $order Associative array containing the order of the meta boxes.
	 * @return mixed[] Associative array containing the revised order of the meta boxes.
	 */
	public function orderPageMetaBoxes($order) {
		$order["normal"] = "postexcerpt,authordiv,slugdiv";
		return $order;
	}	
	
	/**
	 * Sets which meta boxes are hidden by default for the "post" and "page" post types.
	 * @param string[] $hidden Array containing the ids of the div tags of the meta boxes.
	 * @param \WP_Screen $screen The WordPress screen object.
	 * @return string[] The filtered array containing a list of meta boxes to hide by default.
	 */
	public function hideDefaultMetaBoxes($hidden, $screen) {
		if ("post" == $screen->base || "page" == $screen->base) {
			$hidden = array(
				"slugdiv", 
				// "postexcerpt", 
				// "commentstatusdiv", 
				// "commentsdiv", 
				"authordiv", 
				"revisionsdiv",
			);
		}
		return $hidden;     
	}	
	
	/**
	 * Changes the labels of the "page" post type.
	 * @global string $pagenow The current screen (page) in the WordPress administration.
	 * @global WP_Post_Type $wp_post_types The WordPress Post Type object.
	 */	
	public function changePageLabels() {
		global $pagenow;
		global $wp_post_types;
		
		if (in_array($pagenow, ["edit.php", "post.php", "post-new.php"])) {
			$currentSiteName = get_bloginfo("name");
			$labels = &$wp_post_types["page"]->labels;

			$labels->name = "{$currentSiteName}: Pages";
			$labels->add_new_item = "{$currentSiteName}: Add New Page";
			$labels->edit_item = "{$currentSiteName}: Edit Page";
		}
	}	

	/**
	 * Hides the "quick edit" actions link on the page list view. 
	 * @param string[] $actions Array of row action links.
	 * @param WP_Post $post The WordPress post object.
	 * @return string[] Array of row action links with the quick edit item removed.
	 */
	public function hidePageQuickEdit($actions = array(), $post = null) {
		if (isset($actions["inline hide-if-no-js"])) {
			unset($actions["inline hide-if-no-js"]);
		}
		return $actions;
	}	
	
	/**
	 * Prioritizes the position of the SEO meta box relative to other meta boxes.
	 * @return string The priority within the context where the boxes should show.
	 */
	function prioritizeSeoMetaBox() {
		return "low";
	}	

	/**
	 * Changes WordPress notice with the post name and removes "view post" link on selected posts (when post is saved).
	 * @global WP_Post $post The current WordPress post object.
	 * @param mixed[] $messages Associative array of WordPress notices.
	 * @return mixed[] Associative array of WordPress notices with view post link removed.
	 */
	public function changePostSaveMessages($messages) { 
		global $post;
		
		$postTypes = ["manufacturer", "trailer"];
		if (in_array($post->post_type, $postTypes)) {
			$index = array_search($post->post_type, $postTypes);
			$postName = ["trailer manufacturer", "trailer"];
			
			// replace "Post published/updated" notice text with that of the post name 
			$messages["post"] = str_ireplace("post", $postName[$index], $messages["post"]);
			$messages["post"] = array_map("ucfirst", $messages["post"]);
			
			// remove the "view post" link from the notice from the specified post types
			$noLinkPostTypes = ["manufacturer"];
			if (in_array($post->post_type, $noLinkPostTypes)) {
				$messages["post"] = preg_replace("/<a[^>]+>([^<]+)<\/a>/i", "\1", $messages["post"]);
			}
		}
		return $messages;
	}	

}
