<?php

namespace TrailerSalesLib\Admin\PostEdit;

include_once TRAILER_SALES_LIB_PLUGIN_PATH . "admin/post-edit/PostEdit.php";

/**
 * Location
 * ========
 * Customizations for the "location" custom post type on the edit post page in the WordPress administration.
 * @author Eric Den Hollander
 */
class Location extends PostEdit {
	
	/**
	 * Adds actions for creating and saving meta boxes for the "location" custom post type.
	 */
	public function __construct() {	
		add_action("add_meta_boxes", [$this, "addMetaBoxes"]);
		add_action("admin_menu", [$this, "removeMetaBoxes"]);	
		add_filter("get_user_option_meta-box-order_manufacturer", [$this, "orderMetaBoxes"]);		
		add_action("save_post", [$this, "saveManufacturer"]);		
		add_filter("get_sample_permalink_html", [$this, "removePermalink"], 10, 5);	
		add_action("wp_before_admin_bar_render", [$this, "removeAdminBarItems"]);
		add_filter("enter_title_here", [$this, "changeTitlePlaceholderText"]);
	}
	
	/**
	 * Adds meta boxes to the trailer location custom post type in the WordPress administration section.
	 */
	public function addMetaBoxes() {
		add_meta_box("location-info-meta-box", "Trailer Location Information", [$this, "renderInfoMetaBox"], "location", "normal", "high");				
	}

	/**
	 * Removes metaboxes in the WordPress admin for editing the "location" custom post type.
	 */
	public function removeMetaBoxes() {
		remove_meta_box("postcustom", "location", "normal");
	}	
	
	/**
	 * Orders the meta boxes.
	 * @param mixed[] $order Associative array containing the order of the meta boxes.
	 * @return mixed[] Associative array containing the revised order of the meta boxes.
	 */
	public function orderMetaBoxes($order) {
		$order["normal"] = "manufacturer-info-meta-box,slugdiv";
		return $order;
	}	
	/**
	 * Saves custom fields of the location post.
	 * @param string $postId The id of the post.
	 * @return null Aborts the function.
	 */
	public function saveManufacturer($postId) {
		
		if (defined("DOING_AUTOSAVE") && DOING_AUTOSAVE) {
			return;
		}
		if ((! isset($_POST["location_nonce"])) || (! wp_verify_nonce($_POST["location_nonce"], "_location_nonce"))) { 
			return;
		}
		if (! current_user_can("edit_post", $postId)) { 
			return;
		}
		
		// save meta data
		update_post_meta($postId, "location_address", sanitize_text_field($_POST["location_address"]));
		update_post_meta($postId, "location_city", sanitize_text_field($_POST["location_city"]));
		update_post_meta($postId, "location_province", sanitize_text_field($_POST["location_province"]));
		update_post_meta($postId, "location_campground", (isset($_POST["location_campground"]) ? sanitize_text_field($_POST["location_campground"]) : "0"));
	}	
	
	/**
	 * Renders HTML of the location information meta box.
	 * @param WP_Post $post WordPress post object.
	 */
	public function renderInfoMetaBox($post) { 
		wp_nonce_field("_location_nonce", "location_nonce"); 		
		?>
		<table class="form-table trailer-sales-meta-box">
			<tbody>
				<tr>
					<th><label for="location_address">Address</label></th>			
					<td><textarea class="large-text" rows="3" id="location_address" name="location_address"><?php echo esc_html($this->getMeta("location_address")); ?></textarea></td>
				</tr>					
				<tr>				
					<th><label for="location_city">City</label></th>
					<td><input class="regular-text" type="text" name="location_city" id="location_city" value="<?php echo esc_html($this->getMeta("location_city")); ?>"></td>	
				</tr>
				<tr>				
					<th><label for="location_province">Province</label></th>
					<td><input class="regular-text" type="text" name="location_province" id="location_province" value="<?php echo esc_html($this->getMeta("location_province")); ?>"></td>	
				</tr>				
			</tbody>
		</table>
		<hr class="trailer-sales-meta-box">
		<table class="form-table trailer-sales-meta-box">
			<tbody>
				<tr>				
					<th><label for="location_campground">Campground</label></th>
					<td>
						<input class="regular-text" type="checkbox" name="location_campground" id="location_campground" value="1" <?php checked($this->getMeta("location_campground"), 1); ?>>
						This location is a campground
					</td>	
				</tr>			
			</tbody>
		</table>	
		<?php
	}	

	/**
	 * Removes the permalink from the edit post screen.
	 * @param string $return Sample permalink HTML markup.
	 * @param int $postId The ID of the post.
	 * @param string $newTitle New sample permalink title.
	 * @param string $newSlug New sample permalink slug.
	 * @param WP_POST $post WordPress post object.
	 * @return string The changed sample permalink HTML markup.
	 */
	public function removePermalink($return, $postId, $newTitle, $newSlug, $post) {
		if ($post->post_type == "location") {
			$return = "";
		}
		return $return;
	}	
	
	/**
	 * Removes items from the WordPress administration bar.
	 * @global WP_Admin_Bar $wp_admin_bar The WordPress administration bar.
	 * @global WP_Screen $current_screen The WordPress screen object.
	 */
	public function removeAdminBarItems() {
		global $wp_admin_bar;
		global $current_screen;
		
		if ($current_screen->post_type == "location") {			
			$wp_admin_bar->remove_menu("view");
		}
	}		
	
	/**
	 * Changes the title placeholder text of an "manufacturer" custom post type in the WordPress admin.
	 * @param string $title The WordPress default title placeholder text.
	 * @return string The changed title placeholder text for the agent custom post type.
	 */
	public function changeTitlePlaceholderText($title) {
		$screen = get_current_screen();
		if ("location" == $screen->post_type) {
			$title = "Enter name of the trailer location";
		}
		return $title;
	}	

}