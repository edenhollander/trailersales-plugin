<?php

namespace TrailerSalesLib\Admin\PostEdit;

include_once TRAILER_SALES_LIB_PLUGIN_PATH . "admin/post-edit/PostEdit.php";

/**
 * Manufacturer
 * ============
 * Customizations for the "manufacturer" custom post type on the edit post page in the WordPress administration.
 * @author Eric Den Hollander
 */
class Manufacturer extends PostEdit {
	
	/**
	 * Adds actions for creating and saving meta boxes for the "manufacturer" custom post type.
	 */
	public function __construct() {	
		add_action("add_meta_boxes", [$this, "addMetaBoxes"]);
		add_action("admin_menu", [$this, "removeMetaBoxes"]);	
		add_filter("get_user_option_meta-box-order_manufacturer", [$this, "orderMetaBoxes"]);		
		add_action("save_post", [$this, "saveManufacturer"]);		
		add_filter("get_sample_permalink_html", [$this, "removePermalink"], 10, 5);	
		add_action("wp_before_admin_bar_render", [$this, "removeAdminBarItems"]);
		add_filter("enter_title_here", [$this, "changeTitlePlaceholderText"]);
	}
	
	/**
	 * Adds meta boxes to the trailer manufacturer custom post type in the WordPress administration section.
	 */
	public function addMetaBoxes() {
		add_meta_box("manufacturer-info-meta-box", "Trailer Manufacturer Information", [$this, "renderInfoMetaBox"], "manufacturer", "normal", "high");				
	}

	/**
	 * Removes metaboxes in the WordPress admin for editing the "manufacturer" custom post type.
	 */
	public function removeMetaBoxes() {
		remove_meta_box("postcustom", "manufacturer", "normal");
	}	
	
	/**
	 * Orders the meta boxes.
	 * @param mixed[] $order Associative array containing the order of the meta boxes.
	 * @return mixed[] Associative array containing the revised order of the meta boxes.
	 */
	public function orderMetaBoxes($order) {
		$order["normal"] = "manufacturer-info-meta-box,unlimited-post-images-meta-box,slugdiv";
		return $order;
	}	
	/**
	 * Saves custom fields of the manufacturer post.
	 * @param string $postId The id of the post.
	 * @return null Aborts the function.
	 */
	public function saveManufacturer($postId) {
		
		if (defined("DOING_AUTOSAVE") && DOING_AUTOSAVE) {
			return;
		}
		if ((! isset($_POST["manufacturer_nonce"])) || (! wp_verify_nonce($_POST["manufacturer_nonce"], "_manufacturer_nonce"))) { 
			return;
		}
		if (! current_user_can("edit_post", $postId)) { 
			return;
		}
		
		// save meta data
		update_post_meta($postId, "manufacturer_url", sanitize_text_field($_POST["manufacturer_url"]));
		update_post_meta($postId, "manufacturer_order", sanitize_text_field($_POST["manufacturer_order"]));
	}	
	
	/**
	 * Renders HTML of the manufacturer excerpt meta box.
	 * @param WP_Post $post WordPress post object.
	 */
	public function renderInfoMetaBox($post) { 
		wp_nonce_field("_manufacturer_nonce", "manufacturer_nonce"); 		
		?>
		<table class="form-table wfc-meta-box">
			<tbody>
				<tr>				
					<th><label for="manufacturer_url">Website URL</label></th>
					<td>
						<input class="regular-text" type="text" name="manufacturer_url" id="manufacturer_url" value="<?php echo esc_html($this->getMeta("manufacturer_url")); ?>">
						<p class="description">The URL of the website to link to for this trailer manufacturer.</p>
					</td>	
				</tr>				
				<tr>				
					<th><label for="manufacturer_order">Order</label></th>
					<td>
						<input class="regular-text" type="text" name="manufacturer_order" id="manufacturer_order" value="<?php echo esc_html($this->getMeta("manufacturer_order")); ?>">
						<p class="description">The order in which this trailer manufacturer appears in relation to all trailer manufacturers.</p>
					</td>	
				</tr>
			</tbody>
		</table>				
		<?php
	}	

	/**
	 * Removes the permalink from the edit post screen.
	 * @param string $return Sample permalink HTML markup.
	 * @param int $postId The ID of the post.
	 * @param string $newTitle New sample permalink title.
	 * @param string $newSlug New sample permalink slug.
	 * @param WP_POST $post WordPress post object.
	 * @return string The changed sample permalink HTML markup.
	 */
	public function removePermalink($return, $postId, $newTitle, $newSlug, $post) {
		if ($post->post_type == "manufacturer") {
			$return = "";
		}
		return $return;
	}	
	
	/**
	 * Removes items from the WordPress administration bar.
	 * @global WP_Admin_Bar $wp_admin_bar The WordPress administration bar.
	 * @global WP_Screen $current_screen The WordPress screen object.
	 */
	public function removeAdminBarItems() {
		global $wp_admin_bar;
		global $current_screen;
		
		if ($current_screen->post_type == "manufacturer") {			
			$wp_admin_bar->remove_menu("view");
		}
	}		
	
	/**
	 * Changes the title placeholder text of an "manufacturer" custom post type in the WordPress admin.
	 * @param string $title The WordPress default title placeholder text.
	 * @return string The changed title placeholder text for the agent custom post type.
	 */
	public function changeTitlePlaceholderText($title) {
		$screen = get_current_screen();
		if ("manufacturer" == $screen->post_type) {
			$title = "Enter name of the trailer manufacturer";
		}
		return $title;
	}	

}