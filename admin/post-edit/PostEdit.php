<?php

namespace TrailerSalesLib\Admin\PostEdit;

/**
 * Generic post class containing functionality that specific post classes should extend.
 * To be used by child classes on the edit post page in the WordPress administration.
 * @author Eric Den Hollander
 */
abstract class PostEdit {
	
	// abstract method stub
	abstract public function addMetaBoxes();
		
	
	/**
	 * Gets custom field from custom post type meta data.
	 * @global WP_Post $post WordPress post object.
	 * @param string $name The name of the custom field to get.
	 * @return string|boolean The value of the custom field.
	 */
	protected function getMeta($name) {
		global $post;
		$field = get_post_meta($post->ID, $name, true);
		if (!empty($field)) {
			return (is_array($field) ? stripslashes_deep($field) : stripslashes(wp_kses_decode_entities($field)));
		} else {
			return false;
		}
	}	
	
}
