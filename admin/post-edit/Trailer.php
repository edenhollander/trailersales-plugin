<?php

namespace TrailerSalesLib\Admin\PostEdit;

include_once TRAILER_SALES_LIB_PLUGIN_PATH . "admin/post-edit/PostEdit.php";

/**
 * Trailer
 * =======
 * Customizations for the "trailer" custom post type on the edit post page in the WordPress administration.
 * @author Eric Den Hollander
 */
class Trailer extends PostEdit {
	
	/**
	 * Adds actions for creating and saving meta boxes for the "trailer" custom post type.
	 */
	public function __construct() {	
		add_action("admin_enqueue_scripts", [$this, "enqueueAssets"]);		
		add_action("add_meta_boxes", [$this, "addMetaBoxes"]);
		add_action("admin_menu", [$this, "removeMetaBoxes"]);
		add_filter("get_user_option_meta-box-order_trailer", [$this, "orderMetaBoxes"]);			
		add_action("wp_ajax_trailer_upload_photos", [$this, "handleUploadedPhotos"]);
		add_action("save_post", [$this, "saveTrailer"]);
		add_filter("enter_title_here", [$this, "changeTitlePlaceholderText"]);
	}
	
	/**
	 * Enqueues JavaScript and CSS assets needed for the trailer meta boxes.
	 */
	public function enqueueAssets() {
		global $current_screen;
		if ($current_screen->post_type == "trailer") {			
			wp_enqueue_script("trailer-sales-trailer-metabox-js", plugins_url("/trailer-sales-lib/admin/assets/trailer-meta-box.js"), array("jquery"), TRAILER_SALES_LIB_PLUGIN_VERSION, true);
			wp_enqueue_style("trailer-sales-trailer-metabox-css", plugins_url("/trailer-sales-lib/admin/assets/trailer-meta-box.css"), array(), TRAILER_SALES_LIB_PLUGIN_VERSION);
			wp_localize_script("trailer-sales-trailer-metabox-js", "trailerSalesTrailer", array(
				"thumbnailWidth" => get_option("thumbnail_size_w"),
				"thumbnailHeight" => get_option("thumbnail_size_h"),
				"ajaxUrl" => admin_url("admin-ajax.php"),
				"ajaxAction" => "trailer_upload_photos",
			));			
		}
	}	

	/**
	 * Adds meta boxes to the "trailer" custom post type in the WordPress administration section.
	 */
	public function addMetaBoxes() {
		add_meta_box("trailer-display-order-meta-box", "Trailer Display Order", [$this, "renderTrailerDisplayOrderMetaBox"], "trailer", "side", "default");				
		add_meta_box("trailer-feature-sheet-meta-box", "Trailer Feature Sheet", [$this, "renderTrailerFeatureSheetMetaBox"], "trailer", "side", "default");				
		add_meta_box("trailer-price-meta-box", "Trailer Price", [$this, "renderTrailerPriceMetaBox"], "trailer", "normal", "high");		
		add_meta_box("trailer-inventory-meta-box", "Trailer Inventory", [$this, "renderTrailerInventoryMetaBox"], "trailer", "normal", "high");
		add_meta_box("trailer-info-meta-box", "Trailer Information", [$this, "renderTrailerInformationMetaBox"], "trailer", "normal", "high");		
		add_meta_box("trailer-features-meta-box", "Trailer Features", [$this, "renderTrailerFeaturesMetaBox"], "trailer", "normal", "high");		
		add_meta_box("trailer-photos-meta-box", "Trailer Photos", [$this, "renderTrailerPhotosMetaBox"], "trailer", "normal", "high");		
	}	
	
	/**
	 * Removes metaboxes in the WordPress admin for editing the "trailer" custom post type.
	 */
	public function removeMetaBoxes() {
		remove_meta_box("postcustom", "trailer", "normal");
		remove_meta_box("slugdiv", "trailer", "normal");
		remove_meta_box("commentstatusdiv", "trailer", "normal");
		remove_meta_box("commentsdiv", "trailer", "normal");
	}
	
	/**
	 * Orders the meta boxes.
	 * @param mixed[] $order Associative array containing the order of the meta boxes.
	 * @return mixed[] Associative array containing the revised order of the meta boxes.
	 */
	public function orderMetaBoxes($order) {
		$order["normal"] = "trailer-price-meta-box,trailer-location-meta-box,trailer-info-meta-box,trailer-features-meta-box,trailer-photos-meta-box";
		return $order;
	}		

	/**
	 * Changes the title placeholder text of an "trailer" custom post type in the WordPress admin.
	 * @param string $title The WordPress default title placeholder text.
	 * @return string The changed title placeholder text for the agent custom post type.
	 */
	public function changeTitlePlaceholderText($title) {
		$screen = get_current_screen();
		if ("trailer" == $screen->post_type) {
			$title = "Enter name of the trailer (year and make)";
		}
		return $title;
	}
	
	/**
	 * Saves custom fields of the trailer post.
	 * @param string $postId The id of the post.
	 * @return null Aborts the function.
	 */
	public function saveTrailer($postId) {
		
		if (defined("DOING_AUTOSAVE") && DOING_AUTOSAVE) {
			return;
		}
		if ((! isset($_POST["trailer_nonce"])) || (! wp_verify_nonce($_POST["trailer_nonce"], "_trailer_nonce"))) { 
			return;
		}
		if (! current_user_can("edit_post", $postId)) { 
			return;
		}
		
		// save the trailer slug if this is a new trailer (remove hook to avoid infinate loop and add it back again)
		if (! wp_is_post_revision($postId)) {
			remove_action("save_post", [$this, "saveTrailer"]);
			wp_update_post(array(
				"ID" => $postId,
				"post_name" => $_POST["trailer_post_slug"],
			));
			add_action("save_post", [$this, "saveTrailer"]);
		}			
		
		// save meta data
		update_post_meta($postId, "trailer_display_order", sanitize_text_field($_POST["trailer_display_order"]));
		
		update_post_meta($postId, "trailer_price", sanitize_text_field(preg_replace("/[^0-9]/", "", $_POST["trailer_price"])));		
		update_post_meta($postId, "trailer_reduced_price", sanitize_text_field(preg_replace("/[^0-9]/", "", $_POST["trailer_reduced_price"])));		
		update_post_meta($postId, "trailer_sold", isset($_POST["trailer_sold"]) ? $_POST["trailer_sold"] : "No");
		update_post_meta($postId, "trailer_payment_monthly", sanitize_text_field(preg_replace("/[^0-9]/", "", $_POST["trailer_payment_monthly"])));		
		update_post_meta($postId, "trailer_payment_biweekly", sanitize_text_field(preg_replace("/[^0-9]/", "", $_POST["trailer_payment_biweekly"])));		
		update_post_meta($postId, "trailer_payment_biweekly_warranty", sanitize_text_field(preg_replace("/[^0-9]/", "", $_POST["trailer_payment_biweekly_warranty"])));		

		update_post_meta($postId, "trailer_inventory_id", sanitize_text_field($_POST["trailer_inventory_id"]));
		update_post_meta($postId, "trailer_sale_type", sanitize_text_field($_POST["trailer_sale_type"]));
		update_post_meta($postId, "trailer_location", sanitize_text_field($_POST["trailer_location"]));
		update_post_meta($postId, "trailer_road", sanitize_text_field($_POST["trailer_road"]));
		update_post_meta($postId, "trailer_lot", sanitize_text_field($_POST["trailer_lot"]));
		
		update_post_meta($postId, "trailer_make", sanitize_text_field($_POST["trailer_make"]));
		update_post_meta($postId, "trailer_model", sanitize_text_field($_POST["trailer_model"]));	
		update_post_meta($postId, "trailer_year", sanitize_text_field($_POST["trailer_year"]));
		update_post_meta($postId, "trailer_length", sanitize_text_field($_POST["trailer_length"]));
		update_post_meta($postId, "trailer_width", sanitize_text_field($_POST["trailer_width"]));
		update_post_meta($postId, "trailer_bathrooms", sanitize_text_field($_POST["trailer_bathrooms"]));
		update_post_meta($postId, "trailer_bedrooms", sanitize_text_field($_POST["trailer_bedrooms"]));

		update_post_meta($postId, "trailer_features", $this->parseTrailerFeaturePostData());
		update_post_meta($postId, "trailer_feature_photo", $_POST["trailer_feature_photo"]);
		update_post_meta($postId, "trailer_photos", $this->parseTrailerPhotoPostData());
	}

	/**
	 * Parses all the trailer feature data (item names and descriptions) into a JSON string.
	 * @return string JSON encoded building data.
	 */
	private function parseTrailerFeaturePostData() {
		$trailer_feature = [];
		for ($i = 0; $i < count($_POST["trailer_feature_key"]); $i++) {
			$trailer_feature[htmlentities(trim($_POST["trailer_feature_key"][$i]))] = htmlentities(trim($_POST["trailer_feature_value"][$i]));
		}
		return json_encode($trailer_feature);
	}
	
	/**
	 * Parses trailer_photos[] array into a JSON string.
	 * @return string JSON encoded data containing the names of all the photos.
	 */
	private function parseTrailerPhotoPostData() {
		$this->handleDeletedPhotos();
		return json_encode(empty($_POST["trailer_photos"]) ? [] : $_POST["trailer_photos"]);		
	}	
	
	/**
	 * Deletes any photos that have been marked for deletion.
	 * @global WP_Post $post The WordPress post object.
	 */
	private function handleDeletedPhotos() {
		global $post;
		$deletedPhotos = ($_POST["trailer_deleted_photos"] == "" ? [] : explode(",", $_POST["trailer_deleted_photos"]));
		foreach ($deletedPhotos as $deletedPhoto) {
			unlink(TRAILER_SALES_LIB_PHOTOS_PATH . $post->post_name . "/" . $deletedPhoto . "-sm.jpg");
			unlink(TRAILER_SALES_LIB_PHOTOS_PATH . $post->post_name . "/" . $deletedPhoto . "-lg.jpg");
		}
	}

	/**
	 * Ajax receiver handling method for uploaded photos.
	 */
	public function handleUploadedPhotos() {
		if (empty($_FILES)) {
			echo "";
			die();
		}
		
		$photoDirectory = TRAILER_SALES_LIB_PHOTOS_PATH . "/" . $_POST["trailer_post_slug"] . "/";
		if (! file_exists($photoDirectory)) {
			mkdir($photoDirectory, 0755, true);			
		}
		
		$photoNames = [];
		foreach ($_FILES as $photo) {
			
			// check to see if file was uploaded
			if (! is_uploaded_file($photo["tmp_name"])) {
				break;
			}
			
			// create image from uploaded file
			if ($photo["type"] == "image/jpeg") {
				$image = imagecreatefromjpeg($photo["tmp_name"]);
			} else if ($photo["type"] == "image/png") {
				$image = imagecreatefrompng($photo["tmp_name"]);
			} else {
				break; 
			}
			
			// process and save image
			$photoName = uniqid();
			$imageLarge = imagescale($image, get_option("large_size_w"));
			imagejpeg($imageLarge, $photoDirectory . $photoName . "-lg.jpg", 60);			
			$imageSmall = imagescale($image, get_option("thumbnail_size_w"));
			imagejpeg($imageSmall, $photoDirectory . $photoName . "-sm.jpg", 60);
			
			// save photo information
			$photoNames[] = $photoName;
		}
		
		echo (empty($photoNames) ? "" : json_encode($photoNames));
		die();
	}
	
	/**
	 * Renders HTML of the trailer display order meta box.
	 * @param WP_Post $post WordPress post object.
	 */	
	public function renderTrailerDisplayOrderMetaBox($post) { ?>
		<p class="description">The order which this trailer gets displayed on the trailer listings page.</p>
		<p class="description"><strong>Note: </strong>The sorting is done alphanumerically meaning that <code>11</code> will be listed before <code>2</code>. Blanks are displayed first.</p>		
		<input class="medium-text" type="text" name="trailer_display_order" id="trailer_display_order" value="<?php echo esc_html($this->getMeta("trailer_display_order")); ?>">
	<?php
	}		
	
	/**
	 * Renders HTML of the trailer feature sheet meta box.
	 * @param WP_Post $post WordPress post object.
	 */	
	public function renderTrailerFeatureSheetMetaBox($post) {
		$hasBeenSaved = (! empty($post->post_name)); 
		
		if ($hasBeenSaved) {  ?>
			<a class="button" href="<?php echo get_permalink();?>pdf/" target="_blank">View Feature Sheet (PDF)</a>
		<?php } else { ?>
			<p class="description">Trailer needs to be saved first in order to view its feature sheet.</p>
		<?php }
	}
	
	/**
	 * Renders HTML of the trailer price meta box.
	 * @param WP_Post $post WordPress post object.
	 */
	public function renderTrailerPriceMetaBox($post) { 
		wp_nonce_field("_trailer_nonce", "trailer_nonce");
		$price = esc_html($this->getMeta("trailer_price"));
		$reducedPrice = esc_html($this->getMeta("trailer_reduced_price"));
		$monthlyPayment = esc_html($this->getMeta("trailer_payment_monthly"));
		$biweeklyPayment = esc_html($this->getMeta("trailer_payment_biweekly"));
		$biweeklyWarrantyPayment = esc_html($this->getMeta("trailer_payment_biweekly_warranty"));
		?>
		<input type="hidden" name="trailer_post_slug" value="<?php echo (empty($post->post_name) ? uniqid() : $post->post_name); ?>">
		<table class="form-table trailer-sales-meta-box">
			<tbody>
				<tr>
					<th><label for="trailer_price">Price</label></th>
					<td><input class="regular-text" type="text" name="trailer_price" id="trailer_price" value="<?php echo (empty($price) ? "" : number_format($price)); ?>"></td>
				</tr>
				<tr>
					<th><label for="trailer_reduced_price">Reduced Price</label></th>
					<td><input class="regular-text" type="text" name="trailer_reduced_price" id="trailer_reduced_price" value="<?php echo (empty($reducedPrice) ? "" : number_format($reducedPrice)); ?>"></td>
				</tr>
				<tr>
					<th><label for="trailer_sold">Sold</label></th>
					<td><input type="checkbox" name="trailer_sold" id="trailer_sold" value="true" <?php echo (esc_html($this->getMeta("trailer_sold")) == "true" ? "checked" : ""); ?>></td>
				</tr>					
			</tbody>
		</table>
		<hr class="trailer-sales-meta-box">
		<table class="form-table trailer-sales-meta-box">
			<tbody>
				<tr>
					<th><label for="trailer_payment_monthly">Monthly Payment</label></th>
					<td><input class="regular-text" type="text" name="trailer_payment_monthly" id="trailer_payment_monthly" value="<?php echo (empty($monthlyPayment) ? "" : number_format($monthlyPayment)); ?>"></td>
				</tr>
				<tr>
					<th><label for="trailer_payment_biweekly">Biweekly Payment</label></th>
					<td><input class="regular-text" type="text" name="trailer_payment_biweekly" id="trailer_payment_biweekly" value="<?php echo (empty($biweeklyPayment) ? "" : number_format($biweeklyPayment)); ?>"></td>
				</tr>
				<tr>
					<th><label for="trailer_payment_biweekly_warranty">Biweekly with Warranty Payment</label></th>
					<td><input class="regular-text" type="text" name="trailer_payment_biweekly_warranty" id="trailer_payment_biweekly_warranty" value="<?php echo (empty($biweeklyWarrantyPayment) ? "" : number_format($biweeklyWarrantyPayment)); ?>"></td>
				</tr>				
			</tbody>
		</table>			
		<?php
	}	
	
	/**
	 * Renders HTML of the trailer inventory meta box.
	 * @param WP_Post $post WordPress post object.
	 */
	public function renderTrailerInventoryMetaBox($post) {
		$trailerLocation = $this->getMeta("trailer_location");
		$saleType = $this->getMeta("trailer_sale_type");
		$locations = get_posts(array("post_type" => "location", "orderby" => "title", "order" => "ASC"));

		?>
		<table class="form-table trailer-sales-meta-box">
			<tbody>
                <tr>
                    <th><label for="trailer_inventory_id">Inventory ID</label></th>
                    <td><input class="regular-text" type="text" name="trailer_inventory_id" id="trailer_inventory_id" value="<?php echo esc_html($this->getMeta("trailer_inventory_id")); ?>"></td>
                </tr>
                <tr>
                    <th><label for="trailer_sale_type" class="trailer-sales-label-select">Sale Type</label></th>
                    <td>
                        <select name="trailer_sale_type" id="trailer_sale_type">
                            <option value="" disabled>Select Sale Type...</option>
                            <option value="" <?php selected("new", $saleType); ?>>No Sale Type Specified</option>
                            <option value="new" <?php selected("new", $saleType); ?>>New</option>
                            <option value="used" <?php selected("used", $saleType); ?>>Used</option>
                        </select>
                    </td>
                </tr>
                <tr>
					<th><label for="trailer_location" class="trailer-sales-label-select">Location</label></th>
					<td>
						<select name="trailer_location" id="trailer_location">
							<option value="" selected disabled>Select Location...</option>
							<option value="" <?php selected("", $trailerLocation); ?>>No Location Specified</option>							
							<?php foreach ($locations as $location) { ?>
								<option value="<?php echo esc_attr($location->ID); ?>" <?php selected($location->ID, $trailerLocation); ?>><?php echo esc_attr($location->post_title); ?></option>
							<?php } ?>
						</select>	
					</td>
				</tr>					
				<tr>
					<th><label for="trailer_road">Road</label></th>
					<td><input class="regular-text" type="text" name="trailer_road" id="trailer_road" value="<?php echo esc_html($this->getMeta("trailer_road")); ?>"></td>
				</tr>				
				<tr>
					<th><label for="trailer_road">Lot</label></th>
					<td><input class="regular-text" type="text" name="trailer_lot" id="trailer_lot" value="<?php echo esc_html($this->getMeta("trailer_lot")); ?>"></td>
				</tr>				
			</tbody>
		</table>				
		<?php
	}	

	/**
	 * Renders HTML of the trailer information meta box.
	 * @param WP_Post $post WordPress post object.
	 */
	public function renderTrailerInformationMetaBox($post) { 
		?>
		<table class="form-table trailer-sales-meta-box">
			<tbody>
				<tr>
					<th><label for="trailer_make">Make</label></th>
					<td><input class="regular-text" type="text" name="trailer_make" id="trailer_make" value="<?php echo esc_html($this->getMeta("trailer_make")); ?>"></td>
				</tr>				
				<tr>
					<th><label for="trailer_model">Model</label></th>
					<td><input class="regular-text" type="text" name="trailer_model" id="trailer_model" value="<?php echo esc_html($this->getMeta("trailer_model")); ?>"></td>
				</tr>				
				<tr>
					<th><label for="trailer_year">Year</label></th>
					<td><input class="regular-text" type="text" name="trailer_year" id="trailer_year" value="<?php echo esc_html($this->getMeta("trailer_year")); ?>"></td>
				</tr>
				<tr>
					<th><label for="trailer_length">Length</label></th>
					<td><input class="regular-text" type="text" name="trailer_length" id="trailer_length" value="<?php echo esc_html($this->getMeta("trailer_length")); ?>"></td>
				</tr>	
				<tr>
					<th><label for="trailer_width">Width</label></th>
					<td><input class="regular-text" type="text" name="trailer_width" id="trailer_width" value="<?php echo esc_html($this->getMeta("trailer_width")); ?>"></td>
				</tr>
				<tr>
					<th><label for="trailer_bathrooms">Number of Bathrooms</label></th>
					<td><input class="regular-text" type="text" name="trailer_bathrooms" id="trailer_bathrooms" value="<?php echo esc_html($this->getMeta("trailer_bathrooms")); ?>"></td>
				</tr>	
				<tr>
					<th><label for="trailer_bedrooms">Number of Bedrooms</label></th>
					<td><input class="regular-text" type="text" name="trailer_bedrooms" id="trailer_bedrooms" value="<?php echo esc_html($this->getMeta("trailer_bedrooms")); ?>"></td>
				</tr>					
			</tbody>
		</table>				
		<?php
	}	

	/**
	 * Renders HTML of the trailer features meta box.
	 * @param WP_Post $post WordPress post object.
	 */
	public function renderTrailerFeaturesMetaBox($post) { 
		$trailer_features = json_decode($this->getMeta("trailer_features"), true);
		if (empty($trailer_features)) {
			$trailer_features = array(
				"Number of Slideouts" => "",
				"Has Air Conditioning" => "",
				"Has Sunroom" => "",
				"Sunroom Size" => "",
				"Has Deck" => "",
				"Deck Size" => "",
				"Has Shed" => "",
				"Shed Size" => "",
			);
		}
		?>
		<p class="description">
			Trailer features that have blank or empty <strong>descriptions</strong> will not be displayed. Only items that have something in the <strong>description</strong> field will be visible on the website.<br>
			To add a trailer feature, click on the <span class="dashicons dashicons-plus-alt"></span> icon.<br>
			To delete a trailer feature, click on its <span class="dashicons dashicons-trash"></span> icon.<br>
			<br>
		</p>
		<table class="form-table trailer-sales-meta-box trailer-sales-meta-box-list trailer-sales-trailer-features">
			<tbody>
				<tr>
					<th>Title</th><th>Description</th><th><a><span class="dashicons dashicons-plus-alt"></span></a></th></th>
				</tr>					
				<?php 
				if (empty($trailer_features)) { ?>
					<tr>
						<td><input class="regular-text trailer-sales-meta-box-list-key" type="text" name="trailer_feature_key[]"></td>
						<td><input class="large-text trailer-sales-meta-box-list-value" type="text" name="trailer_feature_value[]"></td>
						<td><a><span class="dashicons dashicons-trash"></span></a></td>	
					</tr>					
				<?php } else {
					foreach ($trailer_features as $key => $value) { ?>
						<tr>
							<td><input class="regular-text trailer-sales-meta-box-list-key" type="text" name="trailer_feature_key[]" value="<?php echo $key; ?>"></td>
							<td><input class="large-text trailer-sales-meta-box-list-value" type="text" name="trailer_feature_value[]" value="<?php echo (empty($value) ? "" : $value); ?>"></td>
							<td><a><span class="dashicons dashicons-trash"></span></a></td>	
						</tr>
					<?php } 
				} ?>						
			</tbody>
		</table>
	<?php }
	
	/**
	 * Renders HTML of the photos meta box.
	 * @param WP_Post $post WordPress post object.
	 */
	public function renderTrailerPhotosMetaBox($post) {
		$photos = json_decode(html_entity_decode($this->getMeta("trailer_photos"), ENT_QUOTES), true);
		$featurePhoto = $this->getMeta("trailer_feature_photo");				
		?>
		<table class="form-table trailer-sales-meta-box trailer-sales-photo-upload">
			<tbody><tr><td>
				<button class="button button-primary photo-upload">Upload Photos</button>
				<p class="description">
					Supported image types are: JPG, JPEG, and PNG.<br>
					To delete a photo, click on the <span class="dashicons dashicons-trash"></span> icon.<br>
					The featured photo for the trailer is indicated by a <strong style="color: #0073aa;">blue</strong> border. You can change the featured photo by clicking on the <span class="dashicons dashicons-format-image"></span> icon.
				</p>
				<div class="file-upload"><input type="file" name="photos[]" accept="image/jpeg, image/png" multiple></div>
				<input type="hidden" name="trailer_deleted_photos" value="">
				<input type="hidden" name="trailer_feature_photo" value="<?php echo $featurePhoto; ?>">
			</td></tr></tbody>
		</table>
		<?php 
		if (!empty($photos)) {
			foreach ($photos as $photo) { ?>
				<div class="trailer-sales-photo <?php echo ($photo == $featurePhoto ? "trailer-sales-feature-photo" : ""); ?>">
					<img src="<?php echo TRAILER_SALES_LIB_PHOTOS_URL . $post->post_name . "/" . $photo . "-sm.jpg"; ?>">
					<input type="hidden" name="trailer_photos[]" value="<?php echo $photo; ?>">
					<div>
						<a data-action="delete"><span class="dashicons dashicons-trash"></span></a> 
						<a data-action="set-featured"><span class="dashicons dashicons-format-image"></span></a> 
						<span class="trailer-sales-photo-name"><?php echo $photo; ?></span>
					</div>
				</div>
			<?php } 
		} else { ?>
			<input type="hidden" name="trailer_photos" value="">
		<?php }
	}	

}