/* 
 * trailer-meta-box.js
 * ===================
 * JavaScript file used by the \TrailerSalesLib\Admin\PostEdit\Trailer class of the "trailer" custom post type in the WordPress admin.
 */


jQuery(document).ready(function() {	

	// trailer features meta box "ADD" event handler
	jQuery('.trailer-sales-trailer-features th a').click(function() { 
		jQuery('.trailer-sales-trailer-features tr:last').after('<tr><td><input class="regular-text trailer-sales-meta-box-list-key" type="text" name="trailer_feature_key[]"></td><td><input class="large-text trailer-sales-meta-box-list-value" type="text" name="trailer_feature_value[]"></td><td><a><span class="dashicons dashicons-trash"></span></a></td></tr>');
	});	
	
	// trailer features meta box "DELETE" event handler
	jQuery(document).on('click', '.trailer-sales-trailer-features td a', function() { 
		jQuery(this).closest('tr').remove();
	});			
	
	// photos meta box "SET FEATUARED" event handler
	jQuery(document).on('click', 'a[data-action="set-featured"]', function() {
		var featurePhoto = jQuery(this).closest('.trailer-sales-photo').find('input[name="trailer_photos[]"]').val();	
		jQuery('input[name="trailer_feature_photo"]').val(featurePhoto);	
		jQuery('.trailer-sales-photo').removeClass('trailer-sales-feature-photo');
		jQuery(this).closest('.trailer-sales-photo').addClass('trailer-sales-feature-photo');
	});		
	
	// photos meta box "DELETE" event handler
	jQuery(document).on('click', 'a[data-action="delete"]', function() {
		var deletedPhotos = jQuery('input[name="trailer_deleted_photos"]').val();
		var deletedPhoto = jQuery(this).closest('.trailer-sales-photo').find('input[name="trailer_photos[]"]').val();
		jQuery('input[name="trailer_deleted_photos"]').val(deletedPhotos === '' ? deletedPhoto : deletedPhotos + ',' + deletedPhoto);
		jQuery(this).closest('.trailer-sales-photo').remove();
	});	
	
	// trigger click on the file upload input when the file upload button is clicked
	jQuery('.trailer-sales-photo-upload button.photo-upload').click(function(event) {
		event.preventDefault();
		jQuery('.file-upload input').trigger('click');
	});
	
	// generates a thumbnail image when photos are selected 
	jQuery('.file-upload input').on('change', function(e) {
        if(typeof FileReader === "undefined") { return true; }
        var elem = jQuery(this);
        var files = e.target.files;
        for (var i = 0, f; f = files[i]; i++) {
            if (f.type.match('image.*')) {
                var reader = new FileReader();
                reader.onload = (function(theFile, fileIndex) {
                    return function(e) {
						var trailerPhotoHtml = '<div class="trailer-sales-photo"><img src="' + e.target.result + '" width="' + trailerSalesTrailer.thumbnailWidth +'"></div>';
						if (jQuery('.trailer-sales-photo').length === 0) {
							jQuery('input[name="trailer_photos"]').remove();
							jQuery('.trailer-sales-photo-upload').after(trailerPhotoHtml);
						} else {
							jQuery('.trailer-sales-photo:last').after(trailerPhotoHtml);
						}
                    };
                })(f, i);
                reader.readAsDataURL(f);
            }
        }
		uploadPhotos(files);		
    });

});

/**
 * Uploads photos via ajax.
 * @param {File} files The files from the form.
 * @global {mixed} trailer-salesTrailer Contains WFC localized settings from WordPress. 
 * @returns {undefined}
 */
function uploadPhotos(files) {
    var data = new FormData();
	data.append("action", trailerSalesTrailer.ajaxAction);
	data.append("trailer_post_slug", jQuery('input[name="trailer_post_slug"]').val());
    jQuery.each(files, function(key, value) {
        data.append(key, value);
    });
	
    jQuery.ajax({
        url: trailerSalesTrailer.ajaxUrl,
        type: 'POST',
        data: data,
        cache: false,
        processData: false, 
        contentType: false,
		dataType: 'json',
        success: function(response) {
			if (response !== "") {
				var photoNames = response;
				var lastChild = 1;
				for (var i = photoNames.length - 1; i >= 0; i--) {
					jQuery('.trailer-sales-photo:nth-last-child(' + lastChild + ')').prepend('<input type="hidden" name="trailer_photos[]" value="' + photoNames[i] + '">');
					jQuery('.trailer-sales-photo:nth-last-child(' + lastChild + ') img').after('<div><a data-action="delete"><span class="dashicons dashicons-trash"></span></a><a data-action="set-featured"><span class="dashicons dashicons-format-image"></span></a><span class="trailer-sales-photo-name">' + photoNames[i] + '</span></div>');
					lastChild++;
				}
			}
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.error(textStatus);
        }
    });	
}
