<?php

namespace TrailerSalesLib\Admin;

/**
 * Settings
 * ========
 * Registers and adds the Experience Camping RV Sales settings page in the WordPress administration.
 * @author Eric Den Hollander
 */

class Settings {
	
	const TRAILER_SALES_LIB_SETTINGS_SLUG = "trailer-sales-lib-settings";

	/**
	 * Adds actions for the trailer-sales-lib plugin settings page in the WordPress administration.
	 */	
	public function __construct() {
		
		// add settings page
		add_action("admin_menu", function() {
			add_options_page("Experience Camping RV Sales Settings", "Experience Camping RV Sales Settings", "manage_options", self::TRAILER_SALES_LIB_SETTINGS_SLUG, [$this, "renderHtml"]);
		});
		
		// add capability for saving
		add_filter("option_page_capability_" . self::TRAILER_SALES_LIB_SETTINGS_SLUG, [$this, "overrideCapability"]);			
		
		// regester settings
		add_action("admin_init", function() {
			register_setting("trailer-sales-lib-settings", "trailer_sales_website_url");
			register_setting("trailer-sales-lib-settings", "trailer_sales_multisite_url");			
			
			register_setting("trailer-sales-lib-settings", "trailer_sales_phone_number");
			register_setting("trailer-sales-lib-settings", "trailer_sales_email_address");			
			
			register_setting("trailer-sales-lib-settings", "trailer_sales_address");
			register_setting("trailer-sales-lib-settings", "trailer_sales_city");
			register_setting("trailer-sales-lib-settings", "trailer_sales_province");
			register_setting("trailer-sales-lib-settings", "trailer_sales_postal_code");
		});		
		
		// enqueue assets
		add_action("admin_enqueue_scripts", function($hook) { 
			if ($hook == "settings_page_trailer-sales-lib-settings") {
				wp_enqueue_style("trailer-sales-settings-css", plugins_url("/trailer-sales-lib/admin/assets/settings.css"), array(), TRAILER_SALES_LIB_PLUGIN_VERSION);
			}
		});	
		
		$plugin = plugin_basename(TRAILER_SALES_LIB_PLUGIN_FILE);
		add_filter("plugin_action_links_$plugin", [$this, "addSettingsLink"]);			
	}
	
	/**
	 * Adds a link to the WFC Website Library settings page on the plugin listing page.
	 * @param string[] The list of links associated with the WFC Website Library plugin such as "Deactivate" and "Edit".
	 * @return string[] The list of links with the settings link appended.
	 */
	public function addSettingsLink($links) {
		$settingsLink = "<a href=\"options-general.php?page=" . self::TRAILER_SALES_LIB_SETTINGS_SLUG . "\">Settings</a>";
		array_push($links, $settingsLink);
		return $links;		
	}	
	
	/**
	 * Overrides capability so that anyone that has "manage_trailer_sales_settings" can edit settings.
	 * @return string The overridden capability.
	 */
	public function overrideCapability() {
		return "manage_trailer_sales_settings";
	}	
	
	/**
	 * Renders out the HTML containing the WFC settings.
	 */
	public function renderHtml() { ?>
		<div class="wrap">
			<h1>Experience Camping RV Sales Settings</h1>
			
			<form action="options.php" method="post">
				<?php
				settings_fields("trailer-sales-lib-settings");
				do_settings_sections("trailer-sales-lib-settings");
				?>
				
				<h2>RV Sales Addresses</h2>
				<p class="description">The <?php echo get_bloginfo("name"); ?> website address as displayed for marketing purposes. The Experience Camping website address cannot be edited.</p>
				<table class="form-table">
					<tbody>			
						<tr>
							<th><label for="trailer_sales_website_url">Marketing URL</label></th>			
							<td><input class="regular-text" type="text" id="trailer_sales_website_url" name="trailer_sales_website_url" value="<?php echo get_option("trailer_sales_website_url"); ?>" /></td>
						</tr>	
						<tr>
							<th><label for="trailer_sales_multisite_url">Experience Camping URL</label></th>			
							<td>
								<input type="hidden" name="trailer_sales_multisite_url" value="<?php echo get_bloginfo("url"); ?>" />
								<input class="large-text" type="text" id="trailer_sales_multisite_url" value="<?php echo get_bloginfo("url"); ?>" disabled />
							</td>
						</tr>							
					</tbody>
				</table>					
				
				<h2>Contact Information</h2>
				<p class="description">Experience Camping RV Sales contact information. Used for displaying the phone number and email address on trailer sales pages and on the PDF feature sheets.</p>
				<table class="form-table">
					<tbody>						
						<tr>
							<th><label for="trailer_sales_phone_number">Phone Number</label></th>			
							<td><input class="regular-text" type="text" id="trailer_sales_phone_number" name="trailer_sales_phone_number" value="<?php echo get_option("trailer_sales_phone_number"); ?>" /></td>
						</tr>		
						<tr>
							<th><label for="trailer_sales_email_address">Email Address</label></th>			
							<td><input class="regular-text" type="text" id="trailer_sales_email_address" name="trailer_sales_email_address" value="<?php echo get_option("trailer_sales_email_address"); ?>" /></td>
						</tr>						
					</tbody>
				</table>					
				
				<h2>Address Information</h2>
				<p class="description">Experience Camping RV address information. Used for displaying address information on trailer sales pages and on the PDF feature sheets.</p>
				<table class="form-table">
					<tbody>		
						<tr>
							<th><label for="trailer_sales_address">Address</label></th>			
							<td><textarea class="large-text" rows="3" id="trailer_sales_address" name="trailer_sales_address"><?php echo esc_textarea(get_option("trailer_sales_address")); ?></textarea></td>
						</tr>	
						<tr>
							<th><label for="trailer_sales_city">City</label></th>			
							<td><input class="regular-text" type="text" id="trailer_sales_city" name="trailer_sales_city" value="<?php echo get_option("trailer_sales_city"); ?>" /></td>
						</tr>
						<tr>
							<th><label for="trailer_sales_province">Province</label></th>			
							<td><input class="regular-text" type="text" id="trailer_sales_province" name="trailer_sales_province" value="<?php echo get_option("trailer_sales_province"); ?>" /></td>
						</tr>	
						<tr>
							<th><label for="trailer_sales_postal_code">Postal Code</label></th>			
							<td><input class="regular-text" type="text" id="trailer_sales_postal_code" name="trailer_sales_postal_code" value="<?php echo get_option("trailer_sales_postal_code"); ?>" /></td>
						</tr>							
					</tbody>
				</table>

				<?php submit_button(); ?>
			</form>
		</div>
		<?php		
	}	
}
