<?php

namespace TrailerSalesLib\Admin\PostList;

/**
 * Customizations for the "location" custom post type on the edit list view page in the WordPress administration.
 * @author Eric Den Hollander
 */
class Locations {
	
	/**
	 * Adds actions and filters for customizing the "location" custom post type in the WordPress administration.
	 */		
	public function __construct() {
		add_filter("months_dropdown_results", [$this, "removeFilterMonthsDropdown"]);		
		add_filter("manage_location_posts_columns", [$this, "defineColumns"]);
		add_action("manage_location_posts_custom_column", [$this, "addColumnContent"], 10, 2);
		add_action("post_row_actions", [$this, "removeActions"], 10, 2);	
		add_action("admin_head-edit.php", [$this, "changeUserInterface"]);		
	}
	
	/*
	 * Removes the month filter dropdown that filters on the post date.
	 * @param object $months The months drop-down query results.
	 */
	public function removeFilterMonthsDropdown($months) {
		global $typenow;		
		if ($typenow == "location") {
			$months = [];
		}
		return $months;
	}	

	/**
	 * Defines the columns in the location edit page ("column slug" => "column title").
	 * @param string[] $columns The default WordPress columns.
	 * @return string[] The changed columns with changed titles and new columns added.
	 */
	public function defineColumns($columns) {
		return array(
			"cb" => "<input type=\"checkbox\" />",
			"logo" => "Logo",				
			"title" => "Trailer Location",
			"address" => "Address",
		);
	}
	
	/**
	 * Adds content to the custom columns in the "location" edit page.
	 * @param string $column The slug of the column to output content for.
	 * @param int $postId The id of the post.
	 */
	public function addColumnContent($column, $postId) {
		switch ($column) {
			case "logo":
			case "logo":
				echo get_the_post_thumbnail($postId, [150, 150]);
				break;		
			case "address":
				$address = esc_html(get_post_meta($postId, "location_address", true));
				$city = esc_html(get_post_meta($postId, "location_city", true));
				$province = esc_html(get_post_meta($postId, "location_province", true));
				echo "{$address}, {$city}, {$province}";
				break;			
		}
	}
	
	/**
	 * Removes action items from the edit page in the WordPress administration.
	 * @param mixed[] $actions Associative array containing a list of actions.
	 * @return mixed[] Associative array containing a modified list of actions.
	 */
	public function removeActions($actions) {
		if (get_post_type() == "location") {
			unset($actions["inline hide-if-no-js"]); // remove quick edit link				
			unset($actions["view"]);  // remove the view link
		}
		return $actions;
	}	
	
	/**
	 * Changes the user interface of the edit page in the WordPress administration.
	 * @global string $current_screen The current WordPress administration screen.
	 * @return null Exits function if the WordPress admin screen is not "edit-location".
	 */
	public function changeUserInterface() {   
		global $current_screen;
		if("edit-location" != $current_screen->id) {
			return;
		}
		?>
		<script type="text/javascript">         
			jQuery(document).ready( function($) {
				// make photo column to be a specific width in listings table
				$('.wp-list-table thead th:first').css("width", "150");
			});    
		</script>
		<?php
	}	

}
