<?php

namespace TrailerSalesLib\Admin\PostList;

/**
 * Customizations for the "manufacturer" custom post type on the edit list view page in the WordPress administration.
 * @author Eric Den Hollander
 */
class Manufacturers {
	
	/**
	 * Adds actions and filters for customizing the "manufacturer" custom post type in the WordPress administration.
	 */		
	public function __construct() {
		add_filter("months_dropdown_results", [$this, "removeFilterMonthsDropdown"]);		
		add_filter("manage_manufacturer_posts_columns", [$this, "defineColumns"]);
		add_filter("manage_edit-manufacturer_sortable_columns", [$this, "makeColumnsSortable"]);			
		add_action("manage_manufacturer_posts_custom_column", [$this, "addColumnContent"], 10, 2);
		add_action("pre_get_posts", [$this, "sortColumn"]);	
		add_action("post_row_actions", [$this, "removeActions"], 10, 2);		
		add_action("admin_head-edit.php", [$this, "changeUserInterface"]);
	}
	
	/*
	 * Removes the month filter dropdown that filters on the post date.
	 * @param object $months The months drop-down query results.
	 */
	public function removeFilterMonthsDropdown($months) {
		global $typenow;		
		if ($typenow == "manufacturer") {
			$months = [];
		}
		return $months;
	}	

	/**
	 * Defines the columns in the manufacturer edit page ("column slug" => "column title").
	 * @param string[] $columns The default WordPress columns.
	 * @return string[] The changed columns with changed titles and new columns added.
	 */
	public function defineColumns($columns) {
		return array(
			"cb" => "<input type=\"checkbox\" />",
			"manufacturer_photo" => "Photo",
			"title" => "Trailer Manufacturer",
			"manufacturer_order" => "Order",
		);
	}
	
	/**
	 * Adds content to the custom columns in the "manufacturer" edit page.
	 * @param string $column The slug of the column to output content for.
	 * @param int $postId The id of the post.
	 */
	public function addColumnContent($column, $postId) {
		switch ($column) {
			case "manufacturer_photo":
				echo get_the_post_thumbnail($postId, [250, 150]);
				break;	
			case "manufacturer_order":
				echo esc_html(get_post_meta($postId, "manufacturer_order", true));
				break;			
		}
	}
	
	/**
	 * Makes custom columns in the activity edit page sortable by mapping column slug with their respective orderby query string parameter.
	 * @param string[] $columns Contains the slugs of the column.
	 * @return string[] The columns with their mapped query string orderby parameter.
	 */
	public function makeColumnsSortable($columns) {
		$columns["manufacturer_order"] = "manufacturer_order"; 	
		return $columns;
	}	
	
	/**
	 * Modifies the query to make the custom columns in the activity edit page sortable.
	 * @param WP_Query $query The WordPress query.
	 */
	public function sortColumn($query) {
		if ($query->is_main_query() && $query->get("post_type") == "manufacturer") {		
			$orderBy = $query->get("orderby");
			switch ($orderBy) {
				case "manufacturer_order":
					$query->set("meta_key", "manufacturer_order");
					$query->set("orderby", "meta_value_num");
					break;
			}				
		}
	}	
	
	/**
	 * Removes action items from the edit page in the WordPress administration.
	 * @param mixed[] $actions Associative array containing a list of actions.
	 * @return mixed[] Associative array containing a modified list of actions.
	 */
	public function removeActions($actions) {
		if (get_post_type() == "manufacturer") {
			unset($actions["inline hide-if-no-js"]); // remove quick edit link				
			unset($actions["view"]);  // remove the view link
		}
		return $actions;
	}	
	
	/**
	 * Changes the user interface of the edit page in the WordPress administration.
	 * @global string $current_screen The current WordPress administration screen.
	 * @return null Exits function if the WordPress admin screen is not "edit-manufacturer".
	 */
	public function changeUserInterface() {   
		global $current_screen;
		if("edit-manufacturer" != $current_screen->id) {
			return;
		}
		?>
		<script type="text/javascript">         
			jQuery(document).ready( function($) {
				// make photo column to be a specific width in listings table
				$('.wp-list-table thead th:first').css("width", "250");
			});    
		</script>
		<?php
	}
}
