<?php

namespace TrailerSalesLib\Admin\PostList;

/**
 * Customizations for the "trailer" custom post type on the edit list view page in the WordPress administration.
 * @author Eric Den Hollander
 */
class Trailers {
	
	/**
	 * Adds actions and filters for customizing the "trailer" custom post type in the WordPress administration.
	 */	
	public function __construct() {
		add_action("admin_enqueue_scripts", [$this, "enqueueAssets"]); 		
		add_filter("months_dropdown_results", [$this, "removeFilterMonthsDropdown"]);		
		add_action("restrict_manage_posts", [$this, "addLocationFilterDropdown"]);
		add_filter("parse_query", [$this, "filterLocation"]);			
		add_filter("manage_trailer_posts_columns", [$this, "defineColumns"]);
		add_filter("manage_edit-trailer_sortable_columns", [$this, "makeColumnsSortable"]);			
		add_action("manage_trailer_posts_custom_column", [$this, "addColumnContent"], 10, 2);
		add_action("pre_get_posts", [$this, "sortColumn"]);	
		add_action("post_row_actions", [$this, "removeActions"], 10, 2);			
		add_action("admin_head-edit.php", [$this, "changeUserInterface"]);
	}
	
	/**
	 * Enqueues JavaScript and CSS assets needed for the trailer list view in the WordPress administration.
	 */
	public function enqueueAssets() {
		global $current_screen;
		if ($current_screen->post_type == "trailer") {			
			wp_enqueue_style("trailers-admin-css", plugins_url("/trailer-sales-lib/admin/assets/trailers.css"), array(), TRAILER_SALES_LIB_PLUGIN_VERSION);
		}
	}		
	
	/*
	 * Removes the month filter dropdown that filters on the post date.
	 * @param object $months The months drop-down query results.
	 */
	public function removeFilterMonthsDropdown($months) {
		global $typenow;		
		if ($typenow == "trailer") {
			$months = [];
		}
		return $months;
	}	
	
	/**
	 * Adds a filter dropdown to the "trailer" edit page in the WordPress administration for filtering campground locations.
	 * @global string $typenow The current post type.
	 */
	public function addLocationFilterDropdown() {
		global $typenow;
		
		if ($typenow == "trailer") {
			$locations = get_posts(array("post_type" => "location", "orderby" => "title", "order" => "ASC"));
			$selectedLocation = (isset($_GET["trailer_location"]) ? $_GET["trailer_location"] : "");
			?>
			<select name="trailer_location" id="trailer_location">
				<option value="" <?php selected("", $selectedLocation); ?>>Show All Locations</option>
				<?php foreach ($locations as $location) { ?>
					<option value="<?php echo esc_attr($location->ID); ?>" <?php selected($location->ID, $selectedLocation); ?>><?php echo esc_attr($location->post_title); ?></option>
				<?php } ?>
			</select>	
			<?php
		}
	}

	/**
	 * Filters "trailer" posts by by the location meta field in the "trailer" edit page in the WordPress administration.
	 * @global string $pagenow The current page in the WordPress administration.
	 * @param WP_Query $wpQuery The WordPress query object.
	 */
	public function filterLocation($query) {
		global $pagenow;
		if ($pagenow == "edit.php" && $query->query_vars["post_type"] == "trailer" && isset($_GET["trailer_location"]) && $_GET["trailer_location"] != "") {
			$query->query_vars["meta_key"] = "trailer_location";
			$query->query_vars["meta_value"] = $_GET["trailer_location"];
			$query->query_vars["meta_compare"] = "=";
		}		
	}	
	
	/**
	 * Defines the columns in the "trailer" edit page ("column slug" => "column title").
	 * @param string[] $columns The default WordPress columns.
	 * @return string[] The changed columns with changed titles and new columns added.
	 */
	public function defineColumns($columns) {
		$columns = array(
			"cb" => "<input type=\"checkbox\" />",
			"photo" => "Photo",
			"title" => "Title",
			"inventory_id" => "Inventory ID",
			"sale_type" => "Sale Type",
			"location" => "Location",
			"price" => "Price",
			"sold" => "Sold",
			"display_order" => "Display Order",
			"date" => "Date",
		);
		if ($this->isGoogleAnalyticsPluginActive()) {
			$columns["gadwp_stats"] = "Analytics";
		}
		return $columns;
	}	

	/**
	 * Makes custom columns in the event edit page sortable by mapping column slug with their respective orderby query string parameter.
	 * @param string[] $columns Contains the slugs of the column.
	 * @return string[] The columns with their mapped query string orderby parameter.
	 */
	public function makeColumnsSortable($columns) {
		$columns["inventory_id"] = "trailer_inventory_id";
		$columns["sale_type"] = "trailer_sale_type";
		$columns["price"] = "price";
		$columns["sold"] = "sold";
		$columns["display_order"] = "display_order"; 
		return $columns;
	}
	
	/**
	 * Adds content to the custom columns in the "trailer" edit list view page.
	 * @global WP_Post $post The WordPress trailer post.
	 * @param string $column The slug of the column to output content for.
	 * @param int $postId The id of the post.
	 */
	public function addColumnContent($column, $postId) {
		global $post;
		
		switch ($column) {			
			case "photo":
				$photo = get_post_meta($postId, "trailer_feature_photo", true);
				$photoPath = TRAILER_SALES_LIB_PHOTOS_URL . str_replace("__trashed", "", $post->post_name) . "/";
				if (!empty($photo)) {
					echo "<img src=\"" . $photoPath . $photo . "-sm.jpg\" width=\"150\">";
				}
				break;
			case "location":
				$location = get_post_meta($postId, "trailer_location", true);
				$road = get_post_meta($postId, "trailer_road", true);
				$lot = get_post_meta($postId, "trailer_lot", true);
				echo (empty($location) ? "" : get_the_title($location) . "<br>");
				echo "<span>{$road} {$lot}</span>";
				break;
            case "inventory_id":
                $inventoryId = get_post_meta($postId, "trailer_inventory_id", true);
                echo (empty($inventoryId) ? "" : $inventoryId);
                break;
            case "sale_type":
                $saleType = get_post_meta($postId, "trailer_sale_type", true);
                echo (empty($saleType) ? "" : ucfirst($saleType));
                break;
			case "price":
				$price = get_post_meta($postId, "trailer_price", true);
				$reducedPrice = get_post_meta($postId, "trailer_reduced_price", true);
				$monthlyPrice = get_post_meta($postId, "trailer_payment_monthly", true);
				$biweeklyPrice = get_post_meta($postId, "trailer_payment_biweekly", true);
				$biweeklyWarrantyPrice = get_post_meta($postId, "trailer_payment_biweekly_warranty", true);
				echo (empty($price) ? "" : (empty($reducedPrice) ? "$" . number_format($price) : "<span class=\"strikethru\">$" . number_format($price) . "</span> $" . number_format($reducedPrice)));
				echo (empty($monthlyPrice) ? "" : "<br><span>$" . number_format($monthlyPrice) . " Monthly</span>");
				echo (empty($biweeklyPrice) ? "" : "<br><span>$" . number_format($biweeklyPrice) . " Biweekly</span>");
				echo (empty($biweeklyWarrantyPrice) ? "" : "<br><span>$" . number_format($biweeklyWarrantyPrice) . " Biweekly with Warranty</span>");
				break;
			case "sold":
				$soldTrailer = get_post_meta($postId, "trailer_sold", true);
				echo ($soldTrailer == "true" ? "Sold" : "");
				break;
			case "display_order":
				$displayOrder = get_post_meta($postId, "trailer_display_order", true);
				echo (empty($displayOrder) ? "" : $displayOrder);
				break;
			case "gadwp_stats":
				echo "<a id=\"gadwp-" . $postId . "\" title=\"" . get_the_title($postId) . "\" href=\"#" . $postId . "\ class=\"gadwp-icon dashicons-before dashicons-chart-area\"></a>";
				break;				
		}
	}
	
	/**
	 * Modifies the query to make the custom columns in the rate edit page sortable.
	 * @param WP_Query $query The WordPress query.
	 */
	public function sortColumn($query) {
		if ($query->is_main_query() && $query->get("post_type") == "trailer") {		
			$orderBy = $query->get("orderby");
			switch ($orderBy) {
                case "inventory_id":
                    $query->set("meta_key", "trailer_inventory_id");
                    $query->set("orderby", "meta_value");
                    break;
                case "sale_type":
                    $query->set("meta_key", "trailer_sale_type");
                    $query->set("orderby", "meta_value");
                    break;
                case "price":
					$query->set("meta_key", "trailer_price");
					$query->set("orderby", "meta_value_num");
					break;	
				case "sold":
					$query->set("meta_key", "trailer_sold");
					$query->set("orderby", "meta_value");
					break;
				case "display_order":
					$query->set("meta_key", "trailer_display_order");
					$query->set("orderby", "meta_value");	
					break;
			}				
		}
	}	
	
	/**
	 * Removes action items from the edit page in the WordPress administration.
	 * @param mixed[] $actions Associative array containing a list of actions.
	 * @return mixed[] Associative array containing a modified list of actions.
	 */
	public function removeActions($actions) {
		if (get_post_type() == "trailer") {
			unset($actions["inline hide-if-no-js"]); // remove quick edit link				
		}
		return $actions;
	}		
	
	/**
	 * Changes the user interface of the edit list view page in the WordPress administration.
	 * @global string $current_screen The current WordPress administration screen.
	 * @return null Exits function if the WordPress admin screen is not "edit-office".
	 */
	public function changeUserInterface() {  
		global $current_screen;
		if("edit-trailer" != $current_screen->id) {
			return;
		}
		?>
		<script type="text/javascript">         
			jQuery(document).ready( function($) {
				// make photo column to be a specific width in listings table
				$('.wp-list-table thead th:first').css("width", "150px");				
				$('.wp-list-table thead th:nth-child(3)').css("width", "20%");
			});
		</script>
		<?php
	}
	
	/**
	 * Checks to see if the Google Analytics Dashboard for WordPress plugin is installed and active.
	 * @return boolean If the Google Analytics plugin is installed and active.
	 */
	private function isGoogleAnalyticsPluginActive() {
		if (is_plugin_active("google-analytics-dashboard-for-wp/gadwp.php")) {
			return true;
		}
		return false;
	}		
}
