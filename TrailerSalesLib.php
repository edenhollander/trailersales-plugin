<?php

namespace TrailerSalesLib;

include_once TRAILER_SALES_LIB_PLUGIN_PATH . "custom-post-types/Trailer.php";
include_once TRAILER_SALES_LIB_PLUGIN_PATH . "custom-post-types/Location.php";
include_once TRAILER_SALES_LIB_PLUGIN_PATH . "custom-post-types/Manufacturer.php";
include_once TRAILER_SALES_LIB_PLUGIN_PATH . "admin/Admin.php";

/**
 * Main loader for the "Trailer Sales API" WordPress plugin.
 * @author Eric Den Hollander
 */
class TrailerSalesLib {

	// properties
	
	private $trailerCustomPostType;
	private $locationCustomPostType;
	private $manufacturerCustomPostType;
	
	private $adminCustomizations;	
	

	/**
	 * Loads plugin functionality.
	 */
	public function __construct() {
		$this->addHooks();
		$this->addCustomPostTypes();
		$this->addAdminCustomizations();
	}
	
	/**
	 * Adds any WordPress hooks that is used by the plugin.
	 */
	private function addHooks() {
		register_activation_hook(TRAILER_SALES_LIB_PLUGIN_FILE, [$this, "createPhotoDirectories"]);
		add_action("init", [$this, "removePostTypeSupport"], 100);	
		add_action("admin_bar_menu", [$this, "removeToolbarItems"], 999);	
		add_action("login_enqueue_scripts", [$this, "enqueueLoginAssets"]); 
		add_action("admin_enqueue_scripts", [$this, "enqueueAdminBarAssets"]); 
		add_action("wp_enqueue_scripts", [$this, "enqueueAdminBarAssets"]); 
		add_filter("login_headertext", [$this, "changeLoginLogoTitle"]);
	}	
	
	/**
	 * Registers custom post types.
	 */
	private function addCustomPostTypes() {
		$this->trailerCustomPostType = new CustomPostType\Trailer();
		$this->locationCustomPostType = new CustomPostType\Location();
		$this->manufacturerCustomPostType = new CustomPostType\Manufacturer();
	}
	
	/**
	 * Applies any WordPress administration customizations by instantiating the Admin object.
	 */
	private function addAdminCustomizations() {
		if (is_admin()) {
			$this->adminCustomizations = new Admin\Admin();
		}		
	}	
	
	/**
	 * Removes support of various WordPress features for the specified post types.
	 */
	public function removePostTypeSupport() {
		remove_post_type_support("page", "comments");
	}
	
	/**
	 * Removes items from the WordPress admin bar.
	 * @param WP_Admin_Bar $wpAdminBar The WordPress admin bar object.
	 */
	public function removeToolbarItems($wpAdminBar) {
		$wpAdminBar->remove_node("wp-logo");   // WordPress menu
		$wpAdminBar->remove_menu("comments");  // remove comments
		$wpAdminBar->remove_node("new-post");  // remove new post from New menu
		$wpAdminBar->remove_node("blog-1-n");  // new post on the Sites > Experience Camping menu
		$wpAdminBar->remove_node("blog-1-c");  // manage comments on the Sites > Experience Camping menu
	}	
	
	/**
	 * Enqueues any styles or JavaScript to be used on the WordPress login screen.
	 */
	public function enqueueLoginAssets() {
		if (in_array($GLOBALS["pagenow"], array("wp-login.php", "wp-register.php"))) {
			wp_enqueue_style("trailer-sales-login-css", plugins_url("/trailer-sales-lib/admin/assets/login.css"), array(), TRAILER_SALES_LIB_PLUGIN_VERSION);
		}
	}	
	
	/**
	 * Enqueues JavaScript and CSS assets needed for the WordPress admin bar.
	 */
	public function enqueueAdminBarAssets() {
		wp_enqueue_style("trailer-sales-admin-bar-css", plugins_url("/trailer-sales-lib/admin/assets/admin-bar.css"), array(), TRAILER_SALES_LIB_PLUGIN_VERSION);
	}	
	
	/**
	 * Changes the title tag of the logo on the login page.
	 * @return string The changed title.
	 */
	public function changeLoginLogoTitle() {
		return "Experience Camping";
	}	
	
	/**
	 * Creates directories for storing trailer sales photos.
	 */
	public static function createPhotoDirectories() {
		if (! is_dir(TRAILER_SALES_LIB_PHOTOS_PATH)) {
		   mkdir(TRAILER_SALES_LIB_PHOTOS_PATH, 0775, true);
		}
	}	

}
