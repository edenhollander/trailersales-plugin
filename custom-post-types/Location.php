<?php

namespace TrailerSalesLib\CustomPostType;

/**
 * Trailer Location Custom Post Type
 * =================================
 * Name of "location" with slug of "trailer-locations"
 * @author Eric Den Hollander
 */
class Location {
	
	/**
	 * Registers the "location" custom post type.
	 */
	public function __construct() {
		add_action("init", [$this, "registerPostType"]);
	}
	
	/**
	 * Registers the "location" custom post type.
	 */
	public function registerPostType() {
		$labels = array(
			"name"                  => "Experience Camping RV Sales: Trailer Locations",
			"singular_name"         => "location",
			"menu_name"             => "Trailer Locations",
			"name_admin_bar"        => "Location",
			"archives"              => "Trailer Locations Archives",
			"parent_item_colon"     => "Parent Trailer Location Item:",
			"all_items"             => "All Trailer Locations",
			"add_new_item"          => "Experience Camping RV Sales: Add New Trailer Location",
			"add_new"               => "Add New Location",
			"new_item"              => "New Trailer Location",
			"edit_item"             => "Experience Camping RV Sales: Edit Trailer Location",
			"update_item"           => "Update Trailer Location",
			"view_item"             => "View Trailer Location",
			"view_items"            => "View Trailer Locations",
			"search_items"          => "Search Trailer Locations",
			"not_found"             => "Not found",
			"not_found_in_trash"    => "Not found in Trash",
			"featured_image"        => "Logo",
			"set_featured_image"    => "Set logo",
			"remove_featured_image" => "Remove logo",		
			"insert_into_item"      => "Insert into item",
			"uploaded_to_this_item" => "Uploaded to this item",
			"items_list"            => "Items list",
			"items_list_navigation" => "Items list navigation",
			"filter_items_list"     => "Filter items list",
		);
		$rewrite = array(
			"slug"                  => "location",
			"with_front"            => false,
			"pages"                 => true,
			"feeds"                 => false,
		);
		$args = array(
			"label"                 => "location",
			"description"           => "Experience Camping RV Sales Trailer Locations",
			"labels"                => $labels,
			"supports"              => array("title", "thumbnail", "custom-fields"),
			"hierarchical"          => false,
			"public"                => true,
			"show_ui"               => true,
			"show_in_menu"          => true,
			"menu_position"         => 46,
			"menu_icon"             => "dashicons-location-alt",
			"show_in_admin_bar"     => true,
			"show_in_nav_menus"     => true,
			"can_export"            => true,
			"has_archive"           => true,		
			"exclude_from_search"   => false,
			"publicly_queryable"    => true,
			"rewrite"               => $rewrite,
			"capability_type"       => "post",
		);
		register_post_type("location", $args);		
	}
	
}
