<?php

namespace TrailerSalesLib\CustomPostType;

/**
 * Trailer Manufacturer Custom Post Type
 * =====================================
 * Name of "manufacturer" with slug of "manufacturers"
 * @author Eric Den Hollander
 */
class Manufacturer {
	
	/**
	 * Registers the "manufacturer" custom post type.
	 */
	public function __construct() {
		add_action("init", [$this, "registerPostType"]);
	}
	
	/**
	 * Registers the "manufacturer" custom post type.
	 */
	public function registerPostType() {
		$labels = array(
			"name"                  => "Experience Camping RV Sales: Trailer Manufacturers",
			"singular_name"         => "manufacturer",
			"menu_name"             => "Trailer Manufacturers",
			"name_admin_bar"        => "Manufacturer",
			"archives"              => "Trailer Manufacturer Archives",
			"parent_item_colon"     => "Parent Trailer Manufacturer Item:",
			"all_items"             => "All Trailer Manufacturers",
			"add_new_item"          => "Experience Camping RV Sales: Add New Trailer Manufacturer",
			"add_new"               => "Add New Manufacturer",
			"new_item"              => "New Trailer Manufacturer",
			"edit_item"             => "Experience Camping RV Sales: Edit Trailer Manufacturer",
			"update_item"           => "Update Trailer Manufacturer",
			"view_item"             => "View Trailer Manufacturer",
			"view_items"            => "View Trailer Manufacturers",
			"search_items"          => "Search Trailer Manufacturer",
			"not_found"             => "Not found",
			"not_found_in_trash"    => "Not found in Trash",
			"featured_image"        => "Featured Image",
			"set_featured_image"    => "Set featured image",
			"remove_featured_image" => "Remove featured image",						
			"insert_into_item"      => "Insert into item",
			"uploaded_to_this_item" => "Uploaded to this item",
			"items_list"            => "Items list",
			"items_list_navigation" => "Items list navigation",
			"filter_items_list"     => "Filter items list",
		);
		$rewrite = array(
			"slug"                  => "manufacturer",
			"with_front"            => false,
			"pages"                 => true,
			"feeds"                 => false,
		);
		$args = array(
			"label"                 => "manufacturers",
			"description"           => "Experience Camping RV Sales Trailer Manufacturers",
			"labels"                => $labels,
			"supports"              => array("title", "editor", "thumbnail", "custom-fields"),
			"hierarchical"          => false,
			"public"                => true,
			"show_ui"               => true,
			"show_in_menu"          => true,
			"menu_position"         => 48,
			"menu_icon"             => "dashicons-wts-trailer2",
			"show_in_admin_bar"     => true,
			"show_in_nav_menus"     => true,
			"can_export"            => true,
			"has_archive"           => true,		
			"exclude_from_search"   => false,
			"publicly_queryable"    => true,
			"rewrite"               => $rewrite,
			"capability_type"       => "post",
		);
		register_post_type("manufacturer", $args);		
	}
	
}
