<?php

namespace TrailerSalesLib\CustomPostType;

/**
 * Trailer Sale Custom Post Type
 * =============================
 * Name of "trailer" with slug of "trailer"
 * @author Eric Den Hollander
 */
class Trailer {
	
	/**
	 * Registers the "trailer-sale" custom post type.
	 */
	public function __construct() {
		add_action("init", [$this, "registerPostType"]);
	}
	
	/**
	 * Registers the "trailer" custom post type.
	 */
	public function registerPostType() {
		$labels = array(
			"name"                  => "Experience Camping RV Sales: Trailers For Sale",
			"singular_name"         => "Trailer For Sale",
			"menu_name"             => "Trailer Sales",
			"name_admin_bar"        => "Trailer For Sale",
			"archives"              => "Trailer Sales Archives",
			"parent_item_colon"     => "Parent Trailer For Sale Item:",
			"all_items"             => "All Trailers For Sale",
			"add_new_item"          => "Experience Camping RV Sales: Add New Trailer For Sale",
			"add_new"               => "Add New Trailer",
			"new_item"              => "New Trailer For Sale",
			"edit_item"             => "Experience Camping RV Sales: Edit Trailer For Sale",
			"update_item"           => "Update Trailer For Sale",
			"view_item"             => "View Trailer",
			"view_items"            => "View Trailers",
			"search_items"          => "Search Trailers",
			"not_found"             => "Not found",
			"not_found_in_trash"    => "Not found in Trash",				
			"insert_into_item"      => "Insert into item",
			"uploaded_to_this_item" => "Uploaded to this item",
			"items_list"            => "Items list",
			"items_list_navigation" => "Items list navigation",
			"filter_items_list"     => "Filter items list",
		);
		$rewrite = array(
			"slug"                  => "trailer",
			"with_front"            => false,
			"pages"                 => true,
			"feeds"                 => false,
		);
		$args = array(
			"label"                 => "trailer sales",
			"description"           => "Experience Camping RV Sales",
			"labels"                => $labels,
			"supports"              => array("title", "editor", "custom-fields"),
			"taxonomies"            => array("trailer-campground"),				
			"hierarchical"          => false,
			"public"                => true,
			"show_ui"               => true,
			"show_in_menu"          => true,
			"menu_position"         => 44,
			"menu_icon"             => "dashicons-wts-trailer1",
			"show_in_admin_bar"     => true,
			"show_in_nav_menus"     => true,
			"can_export"            => true,
			"has_archive"           => true,		
			"exclude_from_search"   => false,
			"publicly_queryable"    => true,
			"rewrite"               => $rewrite,
			"capability_type"       => "post",
		);
		register_post_type("trailer", $args);		
	}
	
}
